package com.kabir.access.main.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;

import com.kabir.access.KabirAccessApplication;
import com.kabir.access.main.view.MainView;

public class MainPresenterImpl implements MainPresenter {

    private MainView mainView;

    private final String SHARED_PREFERENCES_FILE_NAME = "kabir_login_preferences";
    private final String KEY_IS_USER_LOGGED_IN = "is_user_logged_in";
    private final String KEY_USER_ID = "user_id";
    private final String KEY_PASSWORD = "password";

    public MainPresenterImpl(@NonNull MainView mainView) {
        this.mainView = mainView;
    }

    @Override
    public void start() {
        Context context;
        try {
            context = KabirAccessApplication.getInstance()
                                            .createPackageContext(
                                                    "com.kabir.login",
                                                    Context.MODE_PRIVATE);
        } catch (PackageManager.NameNotFoundException e) {
            mainView.clearCredentials();
            return;
        }

        SharedPreferences sp = context.getSharedPreferences(SHARED_PREFERENCES_FILE_NAME,
                                                            Activity.MODE_PRIVATE);

        boolean isUserLoggedIn = sp.getBoolean(KEY_IS_USER_LOGGED_IN, false);
        if (!isUserLoggedIn) {
            mainView.clearCredentials();
            return;
        }

        String userId = sp.getString(KEY_USER_ID, null);
        if (userId == null) {
            mainView.clearCredentials();
            return;
        }

        String password = sp.getString(KEY_PASSWORD, null);
        if (password == null) {
            mainView.clearCredentials();
            return;
        }

        mainView.showCredentials(userId, password);
    }
}
