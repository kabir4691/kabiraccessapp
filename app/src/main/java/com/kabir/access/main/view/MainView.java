package com.kabir.access.main.view;

import android.support.annotation.NonNull;

public interface MainView {

    void showCredentials(@NonNull String userId, @NonNull String password);
    void clearCredentials();
}
