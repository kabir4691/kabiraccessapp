package com.kabir.access.main.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.kabir.access.R;
import com.kabir.access.main.presenter.MainPresenter;
import com.kabir.access.main.presenter.MainPresenterImpl;

public class MainActivity extends AppCompatActivity implements MainView {

    private TextView userIdTextView;
    private TextView passwordTextView;

    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        userIdTextView = findViewById(R.id.user_id_text_view);
        passwordTextView = findViewById(R.id.password_text_view);

        presenter = new MainPresenterImpl(this);
        presenter.start();
    }

    @Override
    public void showCredentials(@NonNull String userId, @NonNull String password) {
        userIdTextView.setText(userId);
        passwordTextView.setText(password);
    }

    @Override
    public void clearCredentials() {
        showCredentials("---", "---");
    }
}
