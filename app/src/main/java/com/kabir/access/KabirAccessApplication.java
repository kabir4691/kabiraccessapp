package com.kabir.access;

import android.app.Application;

public class KabirAccessApplication extends Application {

    private static KabirAccessApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static synchronized KabirAccessApplication getInstance() {
        return instance;
    }
}
